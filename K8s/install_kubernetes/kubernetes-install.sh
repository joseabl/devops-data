#!/bin/bash

sudo apt update -y
#install docker
sudo apt-get install docker.io -y
sudo systemctl start docker
sudo systemctl enable docker
docker --version
sudo apt-get install apt-transport-https curl -y
# add Kubernetes package repository key
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
# configure Kubernetes repository
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
sudo apt update
#disable swap temporary
sudo swapoff -a
#Install Kubeadm package
sudo apt-get install kubeadm -y
kubeadm version
#install the parts we need for Kubernetes
sudo apt-get install -y kubelet kubectl kubernetes-cni
#We can now initialize Kubernetes by running the initialization command and passing --pod-network-cidr which is required for Flannel to work correctly
sudo kubeadm init --pod-network-cidr=172.168.10.0/24
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
#check status of node
kubectl get nodes
sudo sysctl net.bridge.bridge-nf-call-iptables=1
sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
#We can check that the pod is up by running
kubectl get pods --all-namespaces
sudo  kubectl get nodes
#install network pod into master node
sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
sudo kubectl get pods --all-namespaces
#join worker node to cluster
kubeadm join --discovery-token abcdef.1234567890abcdef --discovery-token-ca-cert-hash sha256:1234..cdef 1.2.3.4:6443
sudo kubectl get nodes
