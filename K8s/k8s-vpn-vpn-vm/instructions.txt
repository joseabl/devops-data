Description:
A GCE instance connectsed to a GKE cluster through VPN. Internal Load Balancer balances the load
in the cluster.

1.- Create two separate networks (e.g. net-1, net-2); one in each project.
Note: these networks need to be different class. For example:
net-1 -> 10.0.0.0/8 (K8s side)
net-2 -> 192.168.0.0/16 (VM side)

2.- Create the VPN between two projects.
Note: Both VPNs need to be created simultaneously, as we need the remote peer IP address.
Note: In case the networks are created in different regions, "Global" needs to be selected, not "Regional".

3.- Deploy a K8s cluster within the previously created network (e.g. net-1).

4.- Create a VM within the previously created network (e.g. net-2).

5.- Open "icmp" traffic on both sides to test the connection, by creating a firewall rule:
> gcloud compute firewall-rules create PING --allow icmp --network __network__

6.- SSH into the VM and ping the internal IP address of any node in the cluster and vice-versa.
Note: Firewall rule might need to be created to allow icmp traffic.
> gcloud compute firewall-rules create PING --allow icmp --network __network__

A this point there should e communication between the GCE VM and the GKE nodes. Now, we are
goint to create an internal Load Balancer to distribute the load in the cluster.

7.- Create some nginx pods (deployment) and a nodePort type service to map the ports on the node. Use
"svc.yaml" and "depl.yaml" for this purpose.
> gcloud container clusters get-credentials __cluster_name__ --zone __zone__ \ 
--project __project__
> kubectl create -f depl.yaml
> kubectl create -f svc.yaml
Note: the service is going to map the container port to the node port. This port is going to be
random, say 30000.

8.- To create the internal Load Balancer Network Services > Load balancing > CREATE LOAD BALANCER.

9.0.- Click on "Start configuration", under TCP Load Balancing. Select:
- Only between my VMs
- Single region only
- No (TCP)
9.1.- Add the instance group of the cluster as Backends and create a Health Check on port 30000.
9.2.- Set port 30000 for the Frontend port number.
9.3.- Create!
Note: The Load Balancer is going to give an entry point (e.g. 10.0.0.6:30000)

10.- Now, the final test. We need to curl Load Balancer IP address to see if we are getting ngnx landing page.
> curl 10.0.0.6:30000

To make the visuals of the test better, we can access to each pod and change the content of nginx
default index.html by "pod-x"
> kubectl exec -it __pod_name_ bash
> echo "pod-x" > /usr/share/nginx/html/index.html  //x = {1, 2, 3,.., n}

Now we can make th ultimate test by hitting the Load balancer with more then one curl.
> for i in {1..100}; do curl 10.0.0.6:30000; done;
